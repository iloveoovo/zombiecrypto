//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "./Ownable.sol";
import "./SafeMath.sol";

contract ZombieFactory is Ownable {

    event NewZombie(uint zombieId, string name, uint dna);
    using SafeMath for uint256;
    using SafeMath32 for uint32;
    using SafeMath16 for uint16;

    uint private dnaDigits = 16;
    uint public dnaModulus = 10 ** dnaDigits;
    uint public cooldownTime = 1 days;


    struct Zombie{
        string name;
        uint dna;
        uint32 level;
        uint32 readyTime;
        uint16 winCount;
        uint16 lossCount;
    }

    Zombie[] public zombies;

    mapping (uint => address) public zombieToOwner;
    mapping (address => uint) public ownerZombieCount;


    constructor(){
         console.log("Deploying a ZombieFactory");
    }

    function _createZombie(string memory _name, uint _dna) internal {
        uint32 readyTime = uint32(block.timestamp + cooldownTime);
        zombies.push(Zombie(_name,_dna, 1, readyTime, 0, 0));
        uint id = zombies.length - 1;
        zombieToOwner[id] = msg.sender;
        ownerZombieCount[msg.sender]= ownerZombieCount[msg.sender].add(1) ;
        emit NewZombie(id, _name, _dna);
    }

    function createRandomZombie(string memory _name) public {
        require(ownerZombieCount [msg.sender] == 0, "already have a zombie");

        uint randDna = _generatRandomDna(_name);
        console.log("create rand '%s'", randDna);
        _createZombie(_name, randDna);
    }

    function _generatRandomDna(string memory _str) private view returns (uint) {
        uint rand = uint(keccak256(abi.encode(_str)));
        return rand % dnaModulus;
    }
}