//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "./ZombieAttack.sol";
import "./ERC721.sol";
import "./SafeMath.sol";

/// @title 一个管理转移僵尸所有权的合约
/// @author heyijun
/// @dev    符合 OpenZeppelin 对 ERC721 标准草案的实现 
contract ZombieOwnership is ZombieAttack, ERC721{
    
     using SafeMath for uint256;    
     mapping(uint => address) zombieApprovals;


    function balanceOf(address _owner) public override view returns (uint256) {
        // 1. 在这里返回 `_owner` 拥有的僵尸数
        return ownerZombieCount[_owner];

    }

    function ownerOf(uint256 _tokenId) public override view returns (address _owner) {
        // 2. 在这里返回 `_tokenId` 的所有者
        return zombieToOwner[_tokenId];
    }


    function transfer(address _to, uint256 _tokenId) public override onlyOwnerOf(_tokenId) {
        _transfer(msg.sender, _to, _tokenId);
    }

        // 2. 在这里添加方法修饰符
    function approve(address _to, uint256 _tokenId) public override onlyOwnerOf(_tokenId) {
        // 3. 在这里定义方法
        zombieApprovals[_tokenId] = _to;
        emit Approval(msg.sender, _to, _tokenId);   
    }


      function takeOwnership(uint256 _tokenId) public override {
        // 从这里开始
          require(zombieApprovals[_tokenId] == msg.sender);
            address owner = ownerOf(_tokenId);
            _transfer(owner, msg.sender, _tokenId);
    }

    function _transfer(address _from, address _to, uint256 _tokenId) private {
        ownerZombieCount[_from] = ownerZombieCount[_from].sub(1);
        ownerZombieCount[_to] = ownerZombieCount[_to].add(1);
        zombieToOwner[_tokenId] = _to;

        emit Transfer(_from, _to, _tokenId); 

    }





}