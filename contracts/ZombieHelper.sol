//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "./ZombieFeeding.sol";


contract ZombieHelper is ZombieFeeding {
    
    using SafeMath for uint256;
    using SafeMath32 for uint32;
    using SafeMath16 for uint16;

    uint levelUpFee = 0.001 ether;

    modifier aboveLevel(uint _level, uint _zombieId) {
        require(zombies[_zombieId].level >= _level, "current level tow small");
        _;
    }

    function withdraw() external onlyOwner {
        //注意点 1） this 需要转换成 address 类型， address(this)；
        address contractAddress = address(this);
        address payable ownerAddress = payable(owner);
        ownerAddress.transfer(contractAddress.balance);
    }

    function setLevelUpFee(uint _fee) external onlyOwner {
        levelUpFee = _fee;
    }

    function levelUp(uint _zombieId) external payable {
        require(msg.value == levelUpFee, "value should be equal 0.001 ether");
        zombies[_zombieId].level =  zombies[_zombieId].level.add(1) ;
    }

    function changeName(uint _zombieId, string memory _newName) external aboveLevel(2,_zombieId) onlyOwnerOf(_zombieId) {
        zombies[_zombieId].name = _newName;
    }

    function changeDna(uint _zombieId, uint _newDna) external aboveLevel(20,_zombieId) onlyOwnerOf(_zombieId){
        zombies[_zombieId].dna = _newDna;
    }

    function getZombiesByOwner(address _owner) external view returns (uint[] memory){
        uint[] memory result = new uint[](ownerZombieCount[_owner]);
        uint counter = 0;
        for (uint i=0; i < zombies.length; i++) {
            if(zombieToOwner[i] == _owner){
                result[counter] = i;
                counter++;
            }
        }

        return result;

    }

}