import * as dotenv from "dotenv";

import { HardhatUserConfig, task } from "hardhat/config";
import "@nomiclabs/hardhat-etherscan";
import "@nomiclabs/hardhat-waffle";
// import "hardhat-gas-reporter"

dotenv.config();
let env = process.env;

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

const config: HardhatUserConfig = {
  solidity: "0.8.4",
  paths: {
    artifacts: './src/artifacts',
  },
  networks: {
    hardhat: {
      chainId: 1337
    },
    ecs: {
      url: env.ECS_URL,
      accounts: 
        env.ECS_PRIVATE_KEY !== undefined ? [env.ECS_PRIVATE_KEY] : [],
    },
    localhost: {
      url: "http://localhost:8545"
    },
    ropsten: {
      url: env.ROPSTEN_URL || "",
      accounts:
        env.PRIVATE_KEY !== undefined ? [env.PRIVATE_KEY] : [],
    },
  }
};

export default config;
