import { expect } from "chai";
import { ethers } from "hardhat";
import "@nomiclabs/hardhat-ethers"
import { Contract } from "ethers";


describe("Greeter", function () {

  let greeter: Contract;

  before(async function () {
    const Greeter = await ethers.getContractFactory("Greeter");
    greeter = await Greeter.deploy("Hello, world!");
    await greeter.deployed();
    console.log(greeter.address);
  });

  it("#greet", async function () {
    expect(await greeter.greet()).to.equal("Hello, world!");
  });

  it("#setGreeting", async function () {
    const setGreetingTx = await greeter.setGreeting("Hola, mundo!");
    // wait until the transaction is mined
    await setGreetingTx.wait();
    expect(await greeter.greet()).to.equal("Hola, mundo!");
  });
  
});


describe("Zombie", function () {

  let zombieFactory: Contract;

  before(async function () {
    const ZombieFactory = await ethers.getContractFactory("ZombieFactory");
    zombieFactory = await ZombieFactory.deploy();
    await zombieFactory.deployed();
    console.log(zombieFactory.address);
  });


  it("#CreateRandomZombie ", async function () {
    const zombie = await zombieFactory.createRandomZombie("shady");
    // console.log(zombie);
    // expect(await zombieFactory.createRandomZombie("shady")).to.equal("1")
  });
});


